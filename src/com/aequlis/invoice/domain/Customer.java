package com.aequlis.invoice.domain;

/**
 * Created by jamesanto on 7/4/16.
 */
public class Customer {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
