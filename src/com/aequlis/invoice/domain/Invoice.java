package com.aequlis.invoice.domain;

/**
 * Created by jamesanto on 7/4/16.
 */
public class Invoice {
    private Customer customer;
    private Product product;
    private Long quantity;
    private Long price;
}
