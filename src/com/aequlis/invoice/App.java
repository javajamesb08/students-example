package com.aequlis.invoice;

import com.aequlis.invoice.dao.DaoCustomer;
import com.aequlis.invoice.dao.impl.DaoCustomerJdbc;
import com.aequlis.invoice.domain.Customer;
import com.aequlis.invoice.service.CustomerService;
import com.aequlis.invoice.service.impl.CustomerServiceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by jamesanto on 7/4/16.
 */
public class App {

    private static String readOption() throws IOException {
        System.out.println("Customer");
        System.out.println("\t1.1 Create");
        //...
        System.out.println("0 quit");

        System.out.println("Enter option : ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        return reader.readLine();
    }

    public static void main(String[] args) throws IOException {
//        DaoCustomer daoCustomer = new DaoCustomerCollectionsImpl();
        DaoCustomer daoCustomer = new DaoCustomerJdbc();
        CustomerService customerService = new CustomerServiceImpl(daoCustomer);

        String option = null;
        do {
            option = readOption();
            switch (option) {
                case "1.1":
                    //Read customer
                    Customer customer = new Customer();
                    //Set customer values
                    customerService.add(customer);
            }
        } while (!option.equals("0"));

    }
}
