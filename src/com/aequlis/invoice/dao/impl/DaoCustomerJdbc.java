package com.aequlis.invoice.dao.impl;

import com.aequlis.invoice.dao.DaoCustomer;
import com.aequlis.invoice.domain.Customer;

import java.util.List;

/**
 * Created by jamesanto on 7/4/16.
 */
public class DaoCustomerJdbc implements DaoCustomer{
    @Override
    public void add(Customer customer) {
        //insert
    }

    @Override
    public void update(Customer customer) {

    }

    @Override
    public void delete(Long customerId) {

    }

    @Override
    public void get(Long customerId) {

    }

    @Override
    public List<Customer> list() {
        return null;
    }
}
