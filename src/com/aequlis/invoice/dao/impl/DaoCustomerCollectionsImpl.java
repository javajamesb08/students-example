package com.aequlis.invoice.dao.impl;

import com.aequlis.invoice.dao.DaoCustomer;
import com.aequlis.invoice.domain.Customer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jamesanto on 7/4/16.
 */
public class DaoCustomerCollectionsImpl implements DaoCustomer{

    Map<Long, Customer> customers = new HashMap<>();
    private Long idCounter = 0L;

    @Override
    public void add(Customer customer) {
        Long id = nextId();
        customer.setId(id);

        customers.put(id, customer);
    }

    @Override
    public void update(Customer customer) {

    }

    @Override
    public void delete(Long customerId) {
        customers.remove(customerId);
    }

    @Override
    public void get(Long customerId) {

    }

    @Override
    public List<Customer> list() {
        return null;
    }

    private Long nextId() {
        idCounter = idCounter + 1;
        return idCounter;
    }
}
