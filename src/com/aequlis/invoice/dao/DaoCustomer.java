package com.aequlis.invoice.dao;

import com.aequlis.invoice.domain.Customer;

import java.util.List;

/**
 * Created by jamesanto on 7/4/16.
 */
public interface DaoCustomer {
    public void add(Customer customer);
    public void update(Customer customer);
    public void delete(Long customerId);
    public void get(Long customerId);
    public List<Customer> list();
}
