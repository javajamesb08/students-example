package com.aequlis.invoice.service;

import com.aequlis.invoice.domain.Customer;
import com.aequlis.invoice.domain.Invoice;
import com.aequlis.invoice.domain.Product;

/**
 * Created by jamesanto on 7/4/16.
 */
public interface InvoiceService {
    public Invoice generateInvoice(Customer customer, Product product, Long quantity);
}
