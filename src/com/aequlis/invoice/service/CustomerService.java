package com.aequlis.invoice.service;

import com.aequlis.invoice.domain.Customer;

import java.util.List;

/**
 * Created by jamesanto on 7/4/16.
 */
public interface CustomerService {
    public void add(Customer customer);
    public void update(Customer customer);
    public void delete(Long customerId);
    public void get(Long customerId);
    public List<Customer> list();
}
