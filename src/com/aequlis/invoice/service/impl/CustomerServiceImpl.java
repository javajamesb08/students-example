package com.aequlis.invoice.service.impl;

import com.aequlis.invoice.dao.DaoCustomer;
import com.aequlis.invoice.domain.Customer;
import com.aequlis.invoice.service.CustomerService;

import java.util.List;

/**
 * Created by jamesanto on 7/4/16.
 */
public class CustomerServiceImpl implements CustomerService {

    private DaoCustomer daoCustomer;

    public CustomerServiceImpl(DaoCustomer daoCustomer) {
        this.daoCustomer = daoCustomer;
    }

    @Override
    public void add(Customer customer) {
        daoCustomer.add(customer);
    }

    @Override
    public void update(Customer customer) {
        daoCustomer.update(customer);
    }

    @Override
    public void delete(Long customerId) {
        daoCustomer.delete(customerId);
    }

    @Override
    public void get(Long customerId) {
        daoCustomer.get(customerId);
    }

    @Override
    public List<Customer> list() {
        return daoCustomer.list();
    }
}
