package com.aequalis.students;

import java.text.ParseException;
import java.util.List;

/**
 * Created by jamesanto on 7/2/16.
 */
public final class StudentListPrinter {
    public static void printStudentList(List<Student> students) throws ParseException {
        for (Student student : students) {
            System.out.println(student.getName() + "\t\t : " + DateUtil.format(student.getDob()));
        }
    }
}
