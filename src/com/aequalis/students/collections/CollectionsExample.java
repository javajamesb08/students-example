package com.aequalis.students.collections;

import com.aequalis.students.Student;

import java.util.*;

/**
 * Created by jamesanto on 7/2/16.
 */
public class CollectionsExample {
    public static void main(String[] args) {
        Set<Student> students = new HashSet<>();
        Student s1 = new Student("John");
        Student s2 = new Student("John");

        System.out.println(s1.equals(s2));

        students.add(s1);
        students.add(s2);

        for (Student s : students) {
            System.out.println(s);
        }

        Map<String, Integer> studentsMap = new HashMap<>();
        studentsMap.put("John", 20);
        studentsMap.put("Doe", 21);

        for(Map.Entry<String, Integer> student: studentsMap.entrySet()) {
            System.out.println(student.getKey() + " => " + student.getValue());
        }

        System.out.println("---------");

        for(String key: studentsMap.keySet()) {
            Integer age = studentsMap.get(key);
            System.out.println(key+ " => " + age);
        }

        System.out.println("---------");

        for(String key: studentsMap.keySet()) {
            System.out.println(key);
        }

        useMap(studentsMap);
    }

    public static void useMap(Map<String, Integer> map) {

    }
}
