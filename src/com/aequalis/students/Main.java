package com.aequalis.students;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by jamesanto on 7/2/16.
 */
public class Main {

    public static void main(String[] args) throws IOException, ParseException {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        Set<Student> students = new HashSet<>();
        for (int i = 0; i < 2; i++) {
            System.out.println("Enter name : ");
            String name = input.readLine();
            System.out.println("Enter DOB in yyyy-MM-dd format : ");
            String dobString = input.readLine();

            Date date = DateUtil.parse(dobString);

            Calendar dob = Calendar.getInstance();
            dob.setTime(date);

            Student student = new Student("");
            student.setDob(dob);
            student.setName(name);

            students.add(student);
        }

        List<Student> teenageStudents = TeenageFiter.filterTeenageStudents(students);
        StudentListPrinter.printStudentList(teenageStudents);
    }
}
