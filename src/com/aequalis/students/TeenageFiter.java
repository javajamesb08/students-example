package com.aequalis.students;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jamesanto on 7/2/16.
 */
public final class TeenageFiter {

    public static List<Student> filterTeenageStudents(Iterable<Student> input) {
        List<Student> teenageStudents = new ArrayList<>();
        for (Student student : input) {
            int age = AgeCalculator.calculateAge(student.getDob());
            if(age >= 13 && age <= 19) {
                teenageStudents.add(student);
            }
        }
        return teenageStudents;
    }
 }
