package com.aequalis.students;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by jamesanto on 7/2/16.
 */
public final class DateUtil {
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static Date parse(String date) throws ParseException {
        return FORMAT.parse(date);
    }

    public static String format(Calendar calendar) throws ParseException {
        Date date = calendar.getTime();
        return FORMAT.format(date);
    }
}
