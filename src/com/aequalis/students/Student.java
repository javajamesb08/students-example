package com.aequalis.students;

import java.util.Calendar;

/**
 * Created by jamesanto on 7/2/16.
 */
public class Student {
    private String name;
    private Calendar dob;

    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getDob() {
        return dob;
    }

    public void setDob(Calendar dob) {
        this.dob = dob;
    }

    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", dob=" + dob +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        Student other = (Student) obj;
        boolean equals = name.equals(other.getName());
        return equals;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result;
        return result;
    }
}
