package com.aequalis.students;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by jamesanto on 7/2/16.
 */
public final class AgeCalculator {
    public static int calculateAge(Calendar dob) {
        Calendar currentDate = Calendar.getInstance();
        int age = currentDate.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        return age;
    }
}
